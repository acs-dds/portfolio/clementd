<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'clementd');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'clementd');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '49hyF5Cs');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0*XJl`j,g}611;`N~L$wLC&IbEav-rg8-?wVf9R#A7=xI ,*fu`*)Fjc-VQq}(64');
define('SECURE_AUTH_KEY',  '8qhxG}?D 1vVxvtFhv]ushe|Y;Fbg`XXHnU<~P&|7h}Jan:Z=?b(w,V[N;K`OEB-');
define('LOGGED_IN_KEY',    '?u@V,i2h[3_uK%:`ch6R}2*Z <|LB@9mRTff^.@=Z7C<cP_x2=y9v@ir(|dUf0s:');
define('NONCE_KEY',        'x(JBfw^PMy&s*=l^.bI<*Nl yR3O^e2,?RU=RPFSe #O-:eN|]2)kQ1Nenv(3X&j');
define('AUTH_SALT',        'CgMeaU/JDQ23t~r*6C$bG:eKnZzjPrXA0r&<[[jc7:PQ#6TJ:6(i9;}{^{gxzz.m');
define('SECURE_AUTH_SALT', 'Ix,EycF%z-!.zGa&dnwdu1_Sm^lQl3 [fns?T81C }}nG4#r>U;D@j5P^7#P_/Vs');
define('LOGGED_IN_SALT',   ':]HlP@&1ox?#Ej)lv=6S9d]Td`)oX:,Vrr7L%}{2d``Rp/z8qH?JtlCSrg%?`STz');
define('NONCE_SALT',       '1{vq9k9!8P(UjOMl}aecw> xq<Qc]GdviaN_Z0p^;D.sCnbX:g+k/(t)$2HhbKNU');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_Ml373N1y';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');