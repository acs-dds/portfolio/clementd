<?php

	header("Content-type: text/plain");

	function verify($string) {
		$string = trim($string); // on commence par s'assurer qu'il n'y a pas d'espace en début ou fin de phrase
		$camelPlace = strrpos($string, ' camel'); // on cherche le combo espace-chameau, pour être sûr qu'il y a bien chameau en bout de phrase et pas un autre mot qui terminerait par chameau
		$camelLenght = strlen(' camel'); // on calcule la longueur de ce combo espace-chameau
		$stringLenght = strlen($string); // on calcule la longueur de la phrase
		$string = ucfirst($string); // dans tous les cas, on met une majuscule au premier caractère
		if ($camelPlace == $stringLenght - $camelLenght) { // si l'indice contenu dans $placeDuChameau est égal à la longueur de la phrase moins la longueur du combo, alors c'est que ce combo est situé tout à la fin de la phrase
			return $string." !"; // on ajoute donc un point d'exclamation
		} else {
			return $string."."; // sinon un point
		}
	}

	echo verify("Hi, my beautiful penguin").PHP_EOL;
	echo verify("Hi, my beautiful camel");

?>